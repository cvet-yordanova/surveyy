import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyCreateDialogComponent } from './components/survey-create-dialog/survey-create-dialog.component';
import { MaterialModule } from './modules/material.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [SurveyCreateDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  exports: [CommonModule,
    MaterialModule,
    FormsModule]
})
export class SharedModule { }
