import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyCreateDialogComponent } from './survey-create-dialog.component';

describe('SurveyCreateDialogComponent', () => {
  let component: SurveyCreateDialogComponent;
  let fixture: ComponentFixture<SurveyCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
