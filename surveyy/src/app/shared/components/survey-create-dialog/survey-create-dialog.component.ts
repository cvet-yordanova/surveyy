import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SurveyPost } from 'src/app/main/survey-form/models/survey-post.model';

@Component({
  selector: 'app-survey-create-dialog',
  templateUrl: './survey-create-dialog.component.html',
  styleUrls: ['./survey-create-dialog.component.scss']
})
export class SurveyCreateDialogComponent implements OnInit {
  ngOnInit(): void {
  }

  constructor(
    public dialogRef: MatDialogRef<SurveyCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SurveyPost) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
