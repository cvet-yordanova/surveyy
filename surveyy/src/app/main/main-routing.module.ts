import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';


const routes: Routes = [{
  path: '',
  component: MainComponent,
  children: [
    {
      path: 'surveys',
      loadChildren: () => import('../main/survey-form/survey-form.module').then(mod => mod.SurveyFormModule)
    },
    {
      path: '',
      redirectTo: '/auth/login'
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
