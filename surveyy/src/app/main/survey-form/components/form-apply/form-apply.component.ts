import { Component, OnInit, ViewChild } from '@angular/core';

import { QuestionApplyService } from '../../services/question-apply.service';
import { AnswerApply } from '../../models/answer-apply';
import { ActivatedRoute, Router } from '@angular/router';
import { SurveyRequest } from '../../models/survey-request.models';
import { AppliedFormService } from '../../services/applied-form.service';
import { FormApply } from '../../models/form-apply';
import { AppliedAnswerService } from '../../services/applied-answer.service';
import { take } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-form-apply',
  templateUrl: './form-apply.component.html',
  styleUrls: ['./form-apply.component.scss']
})
export class FormApplyComponent implements OnInit {

  answersApplied: Array<AnswerApply> = [];
  survey: SurveyRequest;
  submitted = false;
  appliedForm: FormApply;


  constructor(
    private questionApplyService: QuestionApplyService,
    private route: ActivatedRoute,
    private appliedFormService: AppliedFormService,
    private appliedAnswerService: AppliedAnswerService,
    private matSnackBar: MatSnackBar,
    private router: Router) {

  }

  ngOnInit() {

    this.survey = this.route.snapshot.data.survey;

    if (this.survey.questions.length > 0) {

      this.appliedFormService.getBySurveyId(this.route.snapshot.params.id)
        .subscribe(resAppliedSurvey => {

          if (!resAppliedSurvey) {
            this.appliedForm = new FormApply();
            this.appliedForm.setSurveyId(this.survey._id);
            this.appliedForm.setAnswers([]);
            //set the user backend
          } else {
            this.appliedForm = resAppliedSurvey;
            console.log(this.appliedForm);
          }
        }, err => {
          console.log(err);
        }, () => {
          this.setAppliedAnswers();
        });

    }
  }

  setAppliedAnswers(): void {

    if (this.appliedForm && this.appliedForm.answers) {

      this.survey.questions.forEach(q => {
        let resultAnwer: AnswerApply;
        resultAnwer = FormApply.prototype
          .getAnswerByQuestionId
          .call(this.appliedForm, q._id);


        if (!resultAnwer) {
          resultAnwer = new AnswerApply();
          resultAnwer.setAnswerId('');
          resultAnwer.setQuestionId(q._id);
        }

        this.appliedForm.answers.push(resultAnwer);
      });
    }
  }

  getAppliedAnswer(questionId: string): AnswerApply {

    if (this.appliedForm && this.appliedForm.answers) {
      return this.appliedForm.answers.find(a => {
        return a.questionId === questionId;
      });
    }

  }

  onSend(): void {
    this.appliedFormService
      .save(this.appliedForm)
      .pipe(take(1))
      .subscribe(resForm => {
        this.appliedForm.answers.forEach(a => {
          a.appliedFormId = resForm._id;
          this.appliedAnswerService.save(a)
            .subscribe(res => { }, err => {
              this.router.navigate(['/surveys'])
                .then(() => {
                  this.matSnackBar.open(err, 'Ok', {
                    duration: 2000
                  });
                });
            }, () => {
              this.router.navigate(['/surveys'])
                .then(() => {
                  this.matSnackBar.open('Answers saved!', 'Ok', {
                    duration: 2000
                  });
                });
            });
        });
      });

  }

  saveValue($event): void {

    const answer = this.appliedForm.answers.find(a => {
      return a.questionId === $event.questionId;
    });
    switch ($event.type) {
      case 'ANSWER ID':
        AnswerApply.prototype.setAnswerId.call(answer, $event.answer);
        break;
      case 'TEXT':
        AnswerApply.prototype.setTextValue.call(answer, $event.answer);
        break;
      case 'ANSWER IDS':
        AnswerApply.prototype.toggleAnswerId.call(answer, $event.answer);
        break;
    }

  }

  onSubmit() {

  }

}
