import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionApplyComponent } from './question-apply.component';

describe('QuestionApplyComponent', () => {
  let component: QuestionApplyComponent;
  let fixture: ComponentFixture<QuestionApplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionApplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionApplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
