import { Component, OnInit, Input, Output } from '@angular/core';
import { Question } from '../../../models/question.model';
import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { AnswerApply } from '../../../models/answer-apply';

@Component({
  selector: 'app-question-apply',
  templateUrl: './question-apply.component.html',
  styleUrls: ['./question-apply.component.scss']
})
export class QuestionApplyComponent implements OnInit {
  typeField: string;

  @Input() question: Question;
  @Input() appliedAnswer: AnswerApply;
  @Output() valueChanged: EventEmitter<any> = new EventEmitter<any>();
  value: Subject<string[]>;

  answer: string;
  constructor() { }

  ngOnInit() {
    this.typeField = this.question.answers[0].fieldType.name;
  }

  updateAnswer(event, type: string) {
    this.valueChanged.emit({ answer: event.value, questionId: this.question._id, type });
  }

}
