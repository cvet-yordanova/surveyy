import { Component, OnInit } from '@angular/core';
import { SurveyService } from '../../services/survey.service';
import { SurveyRequest } from '../../models/survey-request.models';
import { take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { SurveyCreateDialogComponent } from 'src/app/shared/components/survey-create-dialog/survey-create-dialog.component';
import { SurveyPost } from '../../models/survey-post.model';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-forms-all',
  templateUrl: './forms-all.component.html',
  styleUrls: ['./forms-all.component.scss']
})
export class FormsAllComponent implements OnInit {

  displayedColumns: string[] = ['title', 'description', 'dateCreated', 'actions'];
  surveys: Array<SurveyRequest> = [];
  dataSource: MatTableDataSource<SurveyRequest>;
  loggedUserId = undefined;

  constructor(
    private surveyService: SurveyService,
    public dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService) { }

  ngOnInit(): void {

    this.surveyService
      .getAll()
      // .pipe(take(1))
      .subscribe(res => {
        this.surveys = res;
        this.dataSource = new MatTableDataSource<SurveyRequest>(this.surveys);
      });

  }

  isLoggedUserAuthor(formUserId: string): boolean {
    console.log(formUserId, localStorage.getItem('userId'));
    return formUserId === localStorage.getItem('userId');
  }

  onCreateSurvey(): void {
    const dialogRef = this.dialog.open(SurveyCreateDialogComponent, {
      width: '250px',
      data: { title: '', description: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      const newSurvey: SurveyPost =
      {
        title: result.title,
        description: result.description,
        dateCreated: new Date()
      };

      this.surveyService.createSurvey(newSurvey)
        .pipe(take(1))
        .subscribe(res => {
          this.router.navigate(['surveys', 'edit', res._id]).then(() => {
            this.snackBar.open('Survey created!', 'ok', {
              duration: 2000
            });
          });
        });

    });
  }

  onDeleteSurvey(id: string): void {
    this.surveyService
      .deleteById(id)
      .pipe(take(1))
      .subscribe(res => {
        let surveyIdx = this.surveys.findIndex(s => s._id === id);
        this.surveys.splice(surveyIdx, 1);
        this.dataSource.data = this.surveys;
      }, err => {
        this.snackBar.open(err, 'Ok', {
          duration: 2000
        });
      }, () => {
        this.snackBar.open('Survey deleted', 'Ok', {
          duration: 2000
        });
      });
  }

}
