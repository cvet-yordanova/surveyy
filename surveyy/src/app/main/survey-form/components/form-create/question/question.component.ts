import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Question } from '../../../models/question.model';
import { Answer } from '../../../models/answer.model';
import { TypeValue } from '../../../models/type-value.model';
import { QuestionsService } from '../../../services/question.service';
import { FieldTypeService } from '../../../services/field-type.service';
import { FieldType } from '../../../models/field-type.model';
import { TypeValueService } from '../../../services/type-value.service';
import { QuestionDbService } from '../../../services/question-db.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() question: Question;
  @Output() deleted: EventEmitter<any> = new EventEmitter();
  typeField: string;
  required: boolean;
  fieldTypeDef: FieldType;
  memoryAnswers: Array<string> = [];
  surveyId: string;
  deletedQuestions: Array<Question> = [];
  deletedAnswers: Array<Answer> = [];

  ANSWERS_DB = {
    shortAnswer: 'Short Answer', freeText: 'Free text', yes: 'Yes', no: 'No', one: '1',
    two: '2', three: '3', four: '4', five: '5', six: '6'
  };


  ngOnInit(): void {

    if (this.question) {
      if (this.question.answers.length > 0) {
        this.typeField = this.question.answers[0].fieldType.name;

      } else {
        this.typeField = this.fieldTypeService.getByName('single-line').name;
      }
      this.required = this.question.required;
      this.fieldTypeDef = this.fieldTypeService.getFieldTypeObj(1);
      this.surveyId = this.question.surveyId;

      if (this.typeField === 'single-choice' || 'multiple-choice' || 'dropdown') {
        this.question.answers.forEach(a => {
          this.memoryAnswers.push(a.value);
        });
      }
    }

  }

  constructor(
    private questionService: QuestionsService,
    private fieldTypeService: FieldTypeService,
    private typeValueService: TypeValueService,
    private questionDbService: QuestionDbService
  ) { }

  getSelectedValue(): string {
    return this.question.answers.length === 0 ?
      'shortText' : this.question.answers[0].fieldType.name;
  }

  onTypeFieldChange(value: string): void {

    this.saveAnswersInMemory();
    this.typeField = value;

    this.question.answers.forEach(a => {
      Answer.prototype.setFieldType.call(a, this.fieldTypeService.getByName(this.typeField));
    });

    let fieldType;
    switch (this.typeField) {
      case 'single-line':
        fieldType = this.fieldTypeService.getByName('single-line');

        this.question.answers[0].value = this.ANSWERS_DB.shortAnswer;

        this.moveDeletedAnswers(1);

        break;
      case 'free-text':

        fieldType = this.fieldTypeService.getByName('free-text');

        this.question.answers[0].value = this.ANSWERS_DB.freeText;

        this.moveDeletedAnswers(1);

        break;
      case 'yes-no':

        fieldType = this.fieldTypeService.getByName('yes-no');

        this.appendAnswersToQuestion(2, fieldType.name);

        this.question.answers[0].value = this.ANSWERS_DB.yes;
        this.question.answers[1].value = this.ANSWERS_DB.no;

        this.moveDeletedAnswers(2);

        break;
      case 'score-card':
        fieldType = this.fieldTypeService.getByName('score-card');

        this.appendAnswersToQuestion(5, fieldType.name);

        this.question.answers[0].value = this.ANSWERS_DB.one;
        this.question.answers[1].value = this.ANSWERS_DB.two;
        this.question.answers[2].value = this.ANSWERS_DB.three;
        this.question.answers[3].value = this.ANSWERS_DB.four;
        this.question.answers[4].value = this.ANSWERS_DB.five;

        this.moveDeletedAnswers(5);

        break;
      case 'single-choice':

        fieldType = this.fieldTypeService.getByName('single-choice');

        this.appendAnswersToQuestion(this.memoryAnswers.length, fieldType.name);

        for (let i = 0; i < this.memoryAnswers.length; i++) {
          this.question.answers[i].value = this.memoryAnswers[i];
        }

        this.moveDeletedAnswers(this.memoryAnswers.length);
        break;
      case 'multiple-choice':

        fieldType = this.fieldTypeService.getByName('multiple-choice');

        this.appendAnswersToQuestion(this.memoryAnswers.length, fieldType.name);

        for (let i = 0; i < this.memoryAnswers.length; i++) {
          this.question.answers[i].value = this.memoryAnswers[i];
        }

        this.moveDeletedAnswers(this.memoryAnswers.length);
        break;
      case 'dropdown':

        fieldType = this.fieldTypeService.getByName('multiple-choice');

        this.appendAnswersToQuestion(this.memoryAnswers.length, fieldType.name);

        for (let i = 0; i < this.memoryAnswers.length; i++) {
          this.question.answers[i].value = this.memoryAnswers[i];
        }

        this.moveDeletedAnswers(this.memoryAnswers.length);
        break;
      default: break;
    }
  }

  saveAnswersInMemory(): void {
    const nonPredifinedAnswers = ['single-choice', 'multiple-choice', 'dropdown'];

    if (nonPredifinedAnswers.includes(this.typeField)) {

      this.memoryAnswers.splice(0, this.memoryAnswers.length);

      for (let i = 0; i < this.question.answers.length; i++) {
        this.memoryAnswers.push(this.question.answers[i].value);
      }
    }
  }

  moveDeletedAnswers(requiredAnswersCount: number): void {

    let counterDeleteAnswers = 0;
    if (this.question.answers.length > requiredAnswersCount) {
      for (let i = requiredAnswersCount; i < this.question.answers.length; i++) {

        if (this.question.answers[i]._id) {
          this.deletedAnswers.push(this.question.answers[i]);
          this.typeValueService.addDeletedAnswer(this.question.answers[i]);
        }
        counterDeleteAnswers++;
      }
    }

    this.question.answers.splice(requiredAnswersCount, counterDeleteAnswers);
  }

  appendAnswersToQuestion(count: number, typeValueName: string): void {

    if (this.question.answers.length < count) {
      const necessaryAnsCount = count - this.question.answers.length;
      for (let i = 0; i < necessaryAnsCount; i++) {
        this.question.answers.push(this.generateAnswer(typeValueName));
      }
    }

  }

  generateAnswer(typeValueName: string): Answer {
    const fieldType = this.fieldTypeService.getByName(typeValueName);

    return new Answer(
      new TypeValue(typeValueName, this.question._id, fieldType._id), fieldType);
  }


  appendQuestion(): void {
    const fieldType = this.fieldTypeService.getByName('single-line');
    const newAnswer = this.generateAnswer(fieldType.name);

    this.questionService.addQuestion(this.question.order + 1,
      new Question({
        order: this.question.order + 1, answers: [newAnswer]
        , surveyId: this.surveyId
      }));
  }

  removeQuestion(): void {
    this.questionService.removeQuestion(this.question.order);
    this.question.answers.forEach(a => {
      this.typeValueService.addDeletedAnswer(a);
    });
    if (this.question._id) {
      this.questionDbService
        .delete(this.question)
        .subscribe(q => {
          console.log(q);
        }, err => {
          console.log(err);
        });
    }
  }

  addAnswer(answerVal: string): void {
    if (answerVal === '') {
      return;
    }
    const newAnswer = this.generateAnswer(this.typeField);
    newAnswer.value = answerVal;

    this.question.answers.push(newAnswer);
    this.questionService.updateQuestion(this.question.order, this.question);
  }

  editTitle(newTitle: string): void {
    if (newTitle === '') {
      return;
    }
    Question.prototype.setName.call(this.question, newTitle);
  }

  setRequired(): void {
    this.required = !this.required;
    Question.prototype
      .setRequired.call(this.question, !this.required);
    this.questionService.updateQuestion(this.question.order, this.question);
  }

  onEditedAnswer($event) {
    const idx = this.memoryAnswers.indexOf($event.oldValue);
    this.memoryAnswers[idx] = $event.newValue;
  }

}
