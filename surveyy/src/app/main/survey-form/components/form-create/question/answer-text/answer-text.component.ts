import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-answer-text',
  templateUrl: './answer-text.component.html',
  styleUrls: ['./answer-text.component.scss']
})
export class AnswerTextComponent {
  @Input() typeText: string;
}
