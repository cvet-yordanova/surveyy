import { OnInit, Input, Component, Output, EventEmitter } from '@angular/core';
import { Question } from 'src/app/main/survey-form/models/question.model';
import { QuestionsService } from 'src/app/main/survey-form/services/question.service';
import { TypeValueService } from 'src/app/main/survey-form/services/type-value.service';
import { Answer } from 'src/app/main/survey-form/models/answer.model';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit {
  @Input() question: Question;
  @Output() editedAnswer = new EventEmitter();
  currentValue: string = '';

  ngOnInit(): void {
  }

  constructor(private questionService: QuestionsService,
    private typeValueService: TypeValueService) { }

  editAnswer(newValue: string, currentValue: string, ansIdx: number): void {
    Question.prototype.editAnswer.call(this.question, ansIdx, newValue);
    // this.question.editAnswer(ansIdx, value);
    this.questionService.updateQuestion(this.question.order, this.question);
    this.editedAnswer.emit({ oldValue: currentValue, newValue });
  }

  removeAnswer(answer: Answer, ansIdx: number): void {
    Question.prototype.removeAnswer.call(this.question, ansIdx);
    // this.question.removeAnswer(ansIdx);
    this.questionService.updateQuestion(this.question.order, this.question);
    this.typeValueService.addDeletedAnswer(answer);
  }

  setCurrentValueOnFocus(currentValue: string): void {
    this.currentValue = currentValue;
  }

  isTypeRadio(fieldTypeName: string): boolean {
    return ['yes-no', 'score-card', 'single-choice'].includes(fieldTypeName);
  }

  isTypeCheckbox(fieldTypeName: string): boolean {
    return 'multiple-choice' === fieldTypeName;
  }

  isTypeDropdown(fieldTypeName: string): boolean {
    return 'drop-down' === fieldTypeName;
  }
}
