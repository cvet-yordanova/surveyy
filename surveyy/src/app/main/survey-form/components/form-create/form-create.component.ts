import { Component, OnInit } from '@angular/core';
import { Question } from '../../models/question.model';
import { Answer } from '../../models/answer.model';
import { QuestionsService } from '../../services/question.service';
import { FieldService } from '../../services/fields.service';
import { FieldTypeService } from '../../services/field-type.service';
import { Observable, pipe, forkJoin } from 'rxjs';
import { TypeValueService } from '../../services/type-value.service';
import { take, map, mergeMap, concatMap } from 'rxjs/operators';
import { SurveyService } from '../../services/survey.service';
import { SurveyRequest } from '../../models/survey-request.models';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionComponent } from './question/question.component';
import { QuestionDbService } from '../../services/question-db.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TypeValue } from '../../models/type-value.model';

@Component({
  selector: 'app-form-create',
  templateUrl: './form-create.component.html',
  styleUrls: ['./form-create.component.scss']
})
export class FormCreateComponent implements OnInit {

  questions: Question[];
  templateId: number;
  activeQuestion: Question;

  survey: SurveyRequest;

  constructor(
    private surveyService: SurveyService,
    private questionService: QuestionsService,
    private typeValueService: TypeValueService,
    private questionDbService: QuestionDbService,
    private fieldTypeService: FieldTypeService,
    private route: ActivatedRoute,
    private matSnackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit(): void {
    this.questionService
      .questionsChanged
      .subscribe(questions => {
        this.questions = questions;
      });
    if (this.route.snapshot.params.id) {
      this.survey = this.route.snapshot.data.survey;
      console.log('******', this.survey, '*******');
      this.questionService.setQuestions(this.survey.questions);
    }
    this.questions = [];

  }

  setActiveIdx(idx: number): void {
    this.questionService.activeQuestionIdx.next(idx);
  }

  generateAnswer(typeValueName: string): Answer {
    const fieldType = this.fieldTypeService.getByName(typeValueName);

    return new Answer(
      new TypeValue(typeValueName, undefined, fieldType._id), fieldType);
  }


  appendFistQuestion(): void {
    const fieldType = this.fieldTypeService.getByName('single-line');
    const newAnswer = this.generateAnswer(fieldType.name);

    this.questionService.addQuestion(1,
      new Question({
        order: 0, answers: [newAnswer]
        , surveyId: this.survey._id
      }));
  }

  addForDeletion(question: Question): void {

  }

  saveQuestions(): void {
    this.typeValueService
      .deleteAnswersInDb()
      .pipe(take(1))
      .subscribe();

    this.questionService.getQuestions()
      .forEach(q => {
        this.questionDbService.save(q)
          .pipe(mergeMap(questRes => {

            const arrRrq = [];
            q.answers.forEach(a => {
              Answer.prototype.setQuestionId.call(a, questRes._id);
              a.fieldTypeId = a.fieldType._id;
              arrRrq.push(this.typeValueService.save(a));
            });

            return forkJoin(arrRrq);

          })).subscribe(
            res => {
              console.log(res);
            },
            err => { },
            () => {
              this.router.navigate(['/surveys'])
                .then(() => {
                  this.matSnackBar.open('Questions saved!', 'Ok', {
                    duration: 2000
                  });
                });
            });
      });
  }
}
