import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Question } from '../models/question.model';

@Injectable({ providedIn: 'root' })
export class QuestionsService {
  questionsChanged = new Subject<Question[]>();
  activeQuestionIdx = new Subject<number>();

  private questions: Question[] = [
    new Question(0)
  ];

  setQuestions(questions: Question[]): void {
    this.questions = questions;
    this.updateQuestionsIdx();
  }

  updateQuestionIdx(idx: number): void {
    this.activeQuestionIdx.next(idx);
  }

  getQuestions(): Question[] {
    return this.questions.slice();
  }

  getQuestionById(id: string): Question {
    return this.questions.find(q => q._id === id);
  }

  getQuestionByIdx(index: number): Question {
    return this.questions[index];
  }

  addQuestion(position: number, question: Question): void {
    this.questions.splice(position, 0, question);
    this.updateQuestionsIdx();
    this.questionsChanged.next(this.questions.slice());
  }

  addQuestionn(position: number, question: Question): void {
    this.questions.splice(position, 0, question);
    this.updateQuestionsIdx();
    this.questionsChanged.next(this.questions.slice());
  }

  updateQuestion(index: number, newQuestion: Question): void {
    this.questions[index] = newQuestion;
    this.questionsChanged.next(this.questions.slice());
  }

  removeQuestion(index: number): void {
    this.questions.splice(index, 1);
    this.updateQuestionsIdx();
    this.questionsChanged.next(this.questions.slice());
  }

  updateQuestionsIdx(): void {
    this.questions.forEach((q, i) => {
      Question.prototype.setOrder.call(q, i);
    });
  }
}
