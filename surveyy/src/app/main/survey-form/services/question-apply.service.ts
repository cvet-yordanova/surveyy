import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AnswerApply } from '../models/answer-apply';


@Injectable()
export class QuestionApplyService {
  answersChanged: Subject<AnswerApply[]>;

  appliedAnswers: Array<AnswerApply> = [];

  constructor() {

  }

  setAppliedAnswers(appliedAnswers: Array<AnswerApply>): void {
    this.appliedAnswers = appliedAnswers;
  }

}
