import { Injectable } from '@angular/core';
import { FieldType } from '../models/field-type.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

@Injectable()
export class FieldTypeService {
  readonly FIELDS_URL = `${environment.heroku}api/field-type`;
  fieldTypes: Array<FieldType> = [];

  constructor(private http: HttpClient) {

  }

  readonly FIELD_TYPES = {
    1: {
      id: 1,
      isPredifined: true,
      name: 'single-line'
    },
    2: {
      id: 2,
      isPredifined: true,
      name: 'multi-line'
    },
    3: {
      id: 3,
      isPredifined: true,
      name: 'score-card'
    },
    4: {
      id: 4,
      isPredifined: true,
      name: 'yes-no'
    },
    5: {
      id: 5,
      isPredifined: false,
      name: 'single-choice'
    },
    6: {
      id: 6,
      isPredifined: false,
      name: 'multiple-choice'
    },
    7: {
      id: 7,
      isPredifined: false,
      name: 'drop-down'
    }
  };

  setAllFieldTypes(): void {
    this.http.get<Array<FieldType>>(`${environment.heroku}api/field-type/`)
      .pipe(take(1))
      .subscribe(resultFieldTypes => {
        this.fieldTypes = resultFieldTypes;
      });
  }

  getById(id: string): FieldType {
    return this.fieldTypes.find(f => {
      return f._id === id;
    });

  }

  getByName(name: string): FieldType {
    return this.fieldTypes.find(f => {
      return f.name === name;
    });
  }

  getFieldTypeId(fieldType: string): number {
    return this.FIELD_TYPES[fieldType];
  }

  getFieldTypeObj(id: number): FieldType {
    return this.FIELD_TYPES[id];
  }

}
