import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FieldRequest } from '../models/field-request.model';

@Injectable()
export class FieldService {
  readonly FIELDS_URL = `${environment.heroku}api/fields`;
  HEADERS: { [key: string]: string } = { 'Content-Type': 'application/json' };

  constructor(private http: HttpClient) { }

  getAll(): Observable<FieldRequest> {
    return this.http.get<FieldRequest>(this.FIELDS_URL);
  }

  getFieldsByFormId(formId: number): Observable<[FieldRequest]> {
    const params = new HttpParams().append('templateFormId', `${formId}`);
    return this.http.get<[FieldRequest]>(this.FIELDS_URL, { params });
  }

  // getFieldsByFormId(formId: number): Observable<Array<FieldRequest>> {
  // 	return this.http.get<Array<FieldRequest>>(this.FIELDS_URL_1).pipe(
  // 		map((fields: Array<FieldRequest>) => {
  // 			return fields.filter(field => {
  // 				return field.templateFormId === formId;
  // 			});
  // 		})
  // 	);
  // }

  saveField(field: FieldRequest): Observable<FieldRequest> {
    if (field._id) {
      return this.http.patch<FieldRequest>(`${this.FIELDS_URL}/${field._id}`, field, { headers: this.HEADERS });
    } else {
      return this.http
        .post<FieldRequest>(this.FIELDS_URL, field, { headers: this.HEADERS });
    }
  }

}
