import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AnswerApply } from '../models/answer-apply';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class AppliedAnswerService {
  readonly APPLIED_ANSWERS_URL = `${environment.heroku}api/applied-answers`;

  constructor(private http: HttpClient) {

  }

  save(appliedAnswer: AnswerApply): Observable<AnswerApply> {
    // console.log(!appliedAnswer._id)

    if (!appliedAnswer._id) {
      return this.http.post<AnswerApply>(`${this.APPLIED_ANSWERS_URL}/`, appliedAnswer);
    } else {
      return this.http.put<AnswerApply>(`${this.APPLIED_ANSWERS_URL}/${appliedAnswer._id}`, appliedAnswer);
    }
  }
}
