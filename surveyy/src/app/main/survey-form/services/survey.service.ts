import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SurveyRequest } from '../models/survey-request.models';
import { SurveyPost } from '../models/survey-post.model';
// import { FormResponse } from '../models/form-response.model';

@Injectable()
export class SurveyService {
  readonly TEMPLATE_FORM_URL = `${environment.heroku}api/surveys`;

  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<SurveyRequest>> {
    return this.http.get<Array<SurveyRequest>>(this.TEMPLATE_FORM_URL);
  }

  getById(id: string): Observable<SurveyRequest> {
    return this.http.get<SurveyRequest>(`${this.TEMPLATE_FORM_URL}/${id}`);
  }

  createSurvey(survey: SurveyPost): Observable<SurveyPost> {
    return this.http.post<SurveyPost>(`${this.TEMPLATE_FORM_URL}`, survey);
  }

  deleteById(id: string): Observable<any> {
    return this.http.delete<any>(`${this.TEMPLATE_FORM_URL}/${id}`);
  }

  // save(form: Form): Observable<SurveyRequest> {
  //   return this.http.get<SurveyRequest>(`${this.TEMPLATE_FORM_URL}`, form);
  // }

  // delete(id: number) {}
}
