import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { FormApply } from '../models/form-apply';

@Injectable()
export class AppliedFormService {
  readonly APPLIED_FORMS_URL = `${environment.heroku}api/applied-forms`;


  constructor(private http: HttpClient) {

  }

  getBySurveyId(surveyId: string): Observable<FormApply> {
    return this.http.get<FormApply>(`${this.APPLIED_FORMS_URL}/${surveyId}`)
      .pipe(tap(res => {
        // console.log(res);
      }));
  }

  save(appliedForm: FormApply): Observable<FormApply> {
    if (!appliedForm._id) {
      return this.http.post<FormApply>(`${this.APPLIED_FORMS_URL}`, appliedForm);
    } else {
      return this.http.put<FormApply>(`${this.APPLIED_FORMS_URL}/${appliedForm._id}`, appliedForm);
    }
  }

}
