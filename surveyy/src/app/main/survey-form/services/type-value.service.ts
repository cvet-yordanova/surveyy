import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TypeValue } from '../models/type-value.model';
import { Answer } from '../models/answer.model';

@Injectable()
export class TypeValueService {
  readonly ANSWERS_URL = `${environment.heroku}api/answers`;
  deletedAnswers: Array<Answer> = [];
  HEADERS: { [key: string]: string } = { 'Content-Type': 'application/json' };

  constructor(private http: HttpClient) { }

  getValuesByFieldId(fieldId: number): Observable<[TypeValue]> {
    const params = new HttpParams().append('fieldId', `${fieldId}`);
    return this.http.get<[TypeValue]>(this.ANSWERS_URL, { params });
  }

  addDeletedAnswer(deletedAnswer: Answer): void {
    this.deletedAnswers.push(deletedAnswer);
  }

  deleteAnswersInDb(): Observable<Answer[]> {
    const deleted = [];
    this.deletedAnswers
      .forEach(a => {
        deleted.push(this.http.delete<Answer>(`${this.ANSWERS_URL}/${a._id}`, this.HEADERS));
      });
    return forkJoin(deleted);

  }

  save(answer: Answer): Observable<Answer> {
    if (answer._id) {
      return this.http.put<Answer>(`${this.ANSWERS_URL}/${answer._id}`, answer);
    } else {
      return this.http
        .post<Answer>(`${this.ANSWERS_URL}/create`, answer);
    }
  }

}
