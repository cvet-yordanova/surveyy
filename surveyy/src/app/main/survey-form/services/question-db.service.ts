import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Question } from '../models/question.model';
import { Observable } from 'rxjs';

@Injectable()
export class QuestionDbService {
  readonly QUESTIONS_URL = `${environment.heroku}api/questions`;

  constructor(private http: HttpClient) { }

  save(question: Question) {
    if (question._id) {
      return this.http.put<Question>(`${this.QUESTIONS_URL}/${question._id}`, question);

    } else {
      return this.http.post<Question>(`${this.QUESTIONS_URL}/create`, question);
    }
  }

  delete(question: Question): Observable<Question> {
    return this.http.delete<Question>(`${this.QUESTIONS_URL}/${question._id}`);
  }

}
