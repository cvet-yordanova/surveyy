import { FormCreateComponent } from './components/form-create/form-create.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsAllComponent } from './components/forms-all/forms-all.component';
import { FormApplyComponent } from './components/form-apply/form-apply.component';
import { SurveyResolver } from './resolvers/survey.resolver';

const routes: Routes = [
  {
    path: '',
    component: FormsAllComponent
  },
  {
    path: 'create',
    component: FormCreateComponent
  },
  {
    path: 'edit/:id',
    component: FormCreateComponent,
    resolve: {
      survey: SurveyResolver
    }
  },
  {
    path: 'apply/:id',
    component: FormApplyComponent,
    resolve: {
      survey: SurveyResolver
    }

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyRoutingModule { }
