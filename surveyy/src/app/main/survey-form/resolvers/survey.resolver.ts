import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { SurveyRequest } from '../models/survey-request.models';
import { SurveyService } from '../services/survey.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FieldTypeService } from '../services/field-type.service';

@Injectable()
export class SurveyResolver implements Resolve<SurveyRequest> {

  constructor(private surveyService: SurveyService, private fieldTypeService: FieldTypeService) {
  }

  resolve(route: ActivatedRouteSnapshot): SurveyRequest
    | Observable<SurveyRequest>
    | Promise<SurveyRequest> {

    this.fieldTypeService.setAllFieldTypes();

    return this.surveyService.getById(route.params.id)
      .pipe(map(resSurvey => {
        resSurvey.questions.forEach(question => {
          question.answers.map(answer => {
            answer.fieldType = this.fieldTypeService.getById(answer.fieldTypeId);
          });
        });

        return resSurvey;
      }));
  }
}
