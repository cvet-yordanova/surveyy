export class TypeValue {
  _id: string;
  value: string;
  fieldId: string;
  fieldTypeId: string;

  constructor(value: string, fieldId: string, fieldTypeId: string) {
    this.value = value;
    this.fieldId = fieldId;
    this.fieldTypeId = fieldTypeId;
  }
}
