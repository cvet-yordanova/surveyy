export class AnswerApply {

  _id?: string;
  appliedFormId?: string;
  questionId: string;
  answerId?: string;
  textValue?: string;
  answerIds?: Array<string>;


  constructor() {
  }

  setQuestionId(questionId: string): void {
    this.questionId = questionId;
  }

  setAnswerId(answerId: string): void {
    this.answerId = answerId;
  }

  setTextValue(textValue: string): void {
    this.textValue = textValue;
  }


  toggleAnswerId(answerId: string) {

    if (!this.answerIds) {
      this.answerIds = [];
    }

    if (this.answerIds.includes(answerId)) {
      const idx = this.answerIds.indexOf(answerId);
      this.answerIds.splice(idx, 1);
    } else {
      this.answerIds.push(answerId);
    }
  }

}
