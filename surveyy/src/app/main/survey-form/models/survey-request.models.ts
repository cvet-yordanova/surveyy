import { Question } from './question.model';

export interface SurveyRequest {
  _id?: string;
  userId: string;
  title: string;
  description: string;
  dateCreated: Date;
  dateUpdated: Date;
  questions: [
    Question
  ];
}
