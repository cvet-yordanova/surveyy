import { AnswerApply } from './answer-apply';

export class FormApply {

  _id?: string;
  answers?: AnswerApply[];
  surveyId: string;
  userId: string;

  constructor() {

  }

  setSurveyId(surveyId: string): void {
    this.surveyId = surveyId;
  }

  setUserId(userId: string): void {
    this.userId = userId;
  }

  setAnswers(answers: AnswerApply[]): void {
    this.answers = answers;
  }

  getAnswerByQuestionId(questionId: string): AnswerApply | boolean {
    return this.answers.find(a => {
      return a.questionId === questionId;
    }) || false;
  }

}
