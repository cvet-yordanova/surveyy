import { SurveyRequest } from './survey-request.models';

export interface FormResponse {
  content: SurveyRequest[];
}
