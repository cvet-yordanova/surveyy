
import { FieldType } from './field-type.model';
import { TypeValue } from './type-value.model';

export class Answer {
  // tslint:disable-next-line: variable-name
  _id?: string;
  value: string;
  questionId: string;
  fieldType: FieldType;
  fieldTypeId?: string;

  constructor(typeValue: TypeValue, fieldType: FieldType) {
    this.value = typeValue.value;
    this.questionId = typeValue.fieldId;
    this.fieldType = fieldType;
  }

  setQuestionId(questionId: string): void {
    this.questionId = questionId;
  }

  setFieldType(fieldType: FieldType): void {
    this.fieldType = fieldType;
  }
}
