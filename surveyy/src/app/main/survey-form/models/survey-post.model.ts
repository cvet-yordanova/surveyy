export interface SurveyPost {
  _id?: string;
  title: string;
  description: string;
  dateCreated: Date;
  dateEdited?: Date;
}
