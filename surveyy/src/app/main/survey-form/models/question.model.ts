import { Answer } from './answer.model';
import { TypeValue } from './type-value.model';

export class Question {
  _id?: string;
  required?: boolean;
  name?: string;
  order?: number;
  answers: Answer[];
  surveyId: string;

  // constructor(order?: number, id?: string, name?: string, required?: boolean) {
  //   this._id = id;
  //   this.order = order;
  //   this.name = name ? name : 'New Question';
  //   this.required = required ? required : false;
  //   this.answers = [];
  // }

  constructor(args) {
    this._id = args.id ? args.id : null;
    this.surveyId = args.surveyId ? args.surveyId : null;
    this.order = args.order ? args.order : 0;
    this.name = args.name ? args.name : 'New Question';
    this.required = args.required ? args.required : false;
    this.answers = args.answers ? args.answers : [];
  }

  setRequired(required: boolean): void {
    this.required = required;
  }

  setName(name: string): void {
    this.name = name;
  }

  setOrder(idx: number): void {
    this.order = idx;
  }

  setAnswers(answers: Answer[]): void {
    this.answers = answers;
  }

  addAnswer(insertIdx: number, answer: Answer): void {
    this.answers.splice(insertIdx, 0, answer);
  }

  editAnswer(idxAnswer: number, newValue: string): void {
    this.answers[idxAnswer].value = newValue;
  }

  removeAnswer(idxAnswer: number): void {
    this.answers.splice(idxAnswer, 1);
  }
}
