export interface FieldRequest {
  _id: string;
  required: boolean;
  name: string;
  order: number;
  templateFormId: string;
}
