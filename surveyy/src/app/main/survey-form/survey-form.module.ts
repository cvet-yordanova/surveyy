import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormCreateComponent } from './components/form-create/form-create.component';
import { FormsAllComponent } from './components/forms-all/forms-all.component';
import { FormApplyComponent } from './components/form-apply/form-apply.component';
import { FormsModule } from '@angular/forms';
import { SurveyRoutingModule } from './survey-routing.module';
import { QuestionComponent } from './components/form-create/question/question.component';
import { AnswerComponent } from './components/form-create/question/answer/answer.component';
import { AnswerTextComponent } from './components/form-create/question/answer-text/answer-text.component';
import { SurveyService } from './services/survey.service';
import { FieldService } from './services/fields.service';
import { TypeValueService } from './services/type-value.service';
import { FieldTypeService } from './services/field-type.service';
import { QuestionDbService } from './services/question-db.service';
import { SurveyCreateDialogComponent } from 'src/app/shared/components/survey-create-dialog/survey-create-dialog.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { QuestionApplyComponent } from './components/form-apply/question-apply/question-apply.component';
import { QuestionApplyService } from './services/question-apply.service';
import { AppliedFormService } from './services/applied-form.service';
import { SurveyResolver } from './resolvers/survey.resolver';
import { AppliedAnswerService } from './services/applied-answer.service';


@NgModule({
  entryComponents: [SurveyCreateDialogComponent],
  declarations: [
    FormCreateComponent,
    FormsAllComponent,
    FormApplyComponent,
    QuestionComponent,
    AnswerComponent,
    AnswerTextComponent,
    QuestionApplyComponent,
  ],
  imports: [
    CommonModule,
    // MaterialModule,
    FormsModule,
    SurveyRoutingModule,
    SharedModule
  ],
  providers: [SurveyService,
    FieldService,
    TypeValueService,
    FieldTypeService,
    QuestionDbService,
    QuestionApplyService,
    AppliedFormService,
    SurveyResolver,
    AppliedAnswerService
  ],

})
export class SurveyFormModule { }
