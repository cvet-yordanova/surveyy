import { CanLoad, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(public auth: AuthService, public router: Router) {

  }

  canLoad(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/', 'auth', 'login']);
      return false;
    }

    return true;
  }

}
