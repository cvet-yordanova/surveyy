import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../services/auth.service';
import { take, takeUntil } from 'rxjs/operators';
import { ValidationService } from 'src/app/auth/services/validation.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  hide = true;

  destroy$ = new Subject<boolean>();

  registerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private validationService: ValidationService) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm(): void {
    this.registerForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        fullName: ['', Validators.required],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      },
      {
        validator: this.validationService.validatePassword
      }
    );

    this.registerForm.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(registerData => {
        this.registerForm.get('confirmPassword').updateValueAndValidity({ emitEvent: false });
      });

  }

  onRegister(): void {
    if (!this.registerForm.valid) {
      return;
    }

    this.authService
      .register(this.registerForm.value)
      .pipe(take(1))
      .subscribe(
        () => {
          this.router.navigate(['auth', 'login']).then(() => {
            this.openSnackBar('Successfull registration!')
          });
        },
        error => {
          console.log(error);
          if (error.status === 409) {
            this.openSnackBar(error.message);
          } else {
            this.openSnackBar('An error occured');
          }
        }
      );
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Ok', {
      duration: 2000
    });
  }

}
