import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const jsonReq = req.clone({
      setHeaders: {
        authorization: 'Bearer ' + JSON.parse(this.authService.getTokenFromStorage()) || '',
      }
    });

    return next.handle(jsonReq);
  }
}
