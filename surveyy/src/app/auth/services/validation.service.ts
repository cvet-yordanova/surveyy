import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  validatePassword(form: FormGroup): null {
    const password = form.get('password').value;
    const confirmPassword = form.get('confirmPassword').value;

    if (password !== confirmPassword) {
      const errors = form.get('confirmPassword').errors !== null ? form.get('confirmPassword').errors : {};
      errors.passwordsDontMatch = true;
      form.get('confirmPassword').setErrors(errors, { emitEvent: true });
    } else {
      if (form.get('confirmPassword').dirty) {
        form.get('confirmPassword').markAsTouched();
      }
      return null;
    }
  }
}
