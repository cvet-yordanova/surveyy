
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { RegisterRequest } from '../models/register-request.model';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginRequest } from '../models/login-request.model';
import { tap } from 'rxjs/operators';
import { LoginResponse } from '../models/login-response.model';
import { environment } from 'src/environments/environment';


@Injectable()
export class AuthService {
  jwtHelper = new JwtHelperService();

  userId = new Subject<string>();


  apiUrl = environment.heroku;

  constructor(private http: HttpClient) {

  }

  register(registerData: RegisterRequest): Observable<{}> {
    return this.http.post(`${this.apiUrl}api/accounts`, registerData);
  }

  login(loginData: LoginRequest): Observable<LoginResponse> {
    return this.http
      .post<LoginResponse>(`${this.apiUrl}api/authenticate`, loginData)
      .pipe(tap(res => {
        if (res) {
          localStorage.setItem('user', loginData.email);
          const decoded = this.jwtHelper.decodeToken(res.token);
          localStorage.setItem('userId', decoded._id);

          this.userId.next(decoded._id);
        }
      }));
  }

  logout(): void {
    this.removeToken();
    localStorage.removeItem('user');
    this.userId.next('');
  }


  saveToken(response: LoginResponse): void {
    const auth = response.token;
    localStorage.setItem('loginKey', JSON.stringify(auth));
  }

  getTokenFromStorage(): string {
    return localStorage.getItem('loginKey');
  }

  removeToken(): void {
    localStorage.removeItem('loginKey');
  }


  isAuthenticated(): boolean {
    const token = this.getTokenFromStorage();

    if (token === null) {
      return false;
    }

    return !this.jwtHelper.isTokenExpired(token);
  }

}
