const mongoose = require("mongoose");

const express = require("express");
const router = require("./config/routes");
const path = require('path');
const app = express();

const cors = require("cors");
const passport = require("passport");

const port = 4000;
const bodyParser = require("body-parser");

mongoose.Promise = global.Promise;
// mongoose.connect("mongodb://127.0.0.1:27017/surveyy", {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// });

mongoose.connect(
  "mongodb+srv://admin:admin@cluster0-fv9z6.mongodb.net/hotel?retryWrites=true&w=majority",
  { useNewUrlParser: true }
);

mongoose.connection.on("connected", function() {
  console.log(
    "Mongoose default connection open to " + "mongodb://127.0.0.1:27017/surveyy"
  );
});

// Serve static files....
app.use(express.static(__dirname + '/surveyy/dist/surveyy'));

// Send all requests to index.html
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/surveyy/dist/surveyy/index.html'));
});

app.listen(process.env.PORT || port, function() {
  console.log("Server started on port " + port);
});

// app.listen(port, () => {
//   console.log(`Server is running at port ${port}`);
// });

require("./config/passport.config")(passport);
app.use(passport.initialize());

app.use(cors());
app.use(express.json());
app.use(bodyParser());
app.use("/api", router);

/////////////////////////////////////////////////////////tests

const FieldType = require("./api/models/field-type");
const Question = require("./api/models/question");
const Answer = require("./api/models/answer");
const Survey = require("./api/models/survey");

async function createSurvey(title, description, dateCreated, dateEdited) {
  const survey = new Survey({
    title,
    description,
    dateCreated
  });

  const result = await survey.save();
}

async function createQuestion(name, order, required) {
  const question = new Question({
    name,
    order,
    required
  });

  const result = await question.save();
}

async function createFieldType(name, isPredifined) {
  const fieldType = new FieldType({
    name,
    isPredifined
  });

  const result = await fieldType.save();
}

async function createAnswer(value, question, fieldType) {
  const answer = new Answer({
    value,
    question,
    fieldType
  });

  const result = await answer.save();
}

// createQuestion("What is your favourite color", 1, true);
// createFieldType("single-choice", false);
// createAnswer("green", "5dd128edd0ccbf1f606d3129", "5dd130afd9b21026807b38cc");
// createSurvey("Random questions", "Description", new Date(), null);
