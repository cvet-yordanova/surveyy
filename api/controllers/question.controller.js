const Question = require("../models/question");
const Answers = require("../models/answer");
const mongoose = require("mongoose");

module.exports = {
  findAll: async function(req, res) {
    if (mongoose.connection.readyState == 1) {
      let result = await mongoose.connection.db
        .collection("questions")
        .aggregate([
          {
            $lookup: {
              from: "answers",
              localField: "_id",
              foreignField: "question",
              as: "answers"
            }
          }
        ])
        .toArray();

      res.json(result);
    }
  },
  deleteByFormIdCascade: function(formId) {
    let getQuestionsByForm;
  },
  create: function(req, res) {
    const { name, order, required, surveyId } = req.body;
    Question.create({ name, order, required, surveyId })
      .then(question => {
        res.json(question);
      })
      .catch(err => res.status(500).json(err));
  },
  save: function(req, res) {
    //todo validation
    const { id } = req.params;
    Question.findByIdAndUpdate({ _id: id }, req.body, { new: true })
      .then(invoice => res.json(invoice))
      .catch(err => res.status(500).json(err));
  },
  delete: function(req, res) {
    const { id } = req.params;
    Question.findByIdAndDelete(id)
      .then(question => res.status(200).json(question))
      .catch(err => res.status(500).json(err));
  }
};

//when deleting question or form
function getAnswerByQuestionId(answerId) {
  return Answers.find({
    question: { $eq: answerId }
  });
}
