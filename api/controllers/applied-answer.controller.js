const AppliedAnswer = require("../models/applied-answer");

module.exports = {
  findByAppliedFormId: function(appliedFormId) {
    return AppliedAnswer.find({ appliedFormId: { $eq: appliedFormId } });
  },
  create: function(req, res) {
    let { questionId, appliedFormId } = req.body;

    let answerId = req.body.answerId ? req.body.answerId : null;
    let textValue = req.body.textValue ? req.body.textValue : null;
    let answerIds = req.body.answerIds ? req.body.answerIds : null;

    AppliedAnswer.create({
      questionId,
      answerId,
      appliedFormId,
      textValue,
      answerIds
    })
      .then(answerAppl => res.json(answerAppl))
      .catch(err => res.status(500).json(err));
  },
  update: function(req, res) {
    let id = req.params.id;

    AppliedAnswer.findByIdAndUpdate(id, req.body)
      .then(resAnswer => {
        res.status(200).json(resAnswer);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  }
};
