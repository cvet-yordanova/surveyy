const AppliedForm = require("../models/applied-form");
const appliedAnswersController = require("../controllers/applied-answer.controller");

module.exports = {
  findBySurveyId: function(req, res) {
    const surveyId = req.params.surveyId;
    const userId = req._id;

    AppliedForm.findOne({
      surveyId: { $eq: surveyId },
      userId: { $eq: userId }
    }).then((resForm, err) => {
      if (err) {
        return res.status(500).json(err);
      }

      if (!resForm) {
        res.status(200).json(false);
      }

      appliedAnswersController
        .findByAppliedFormId(resForm._id)
        .then((resAnswers, err) => {
          if (err) {
            res.status(500).json(err);
          }

          res.status(200).json({ ...resForm._doc, answers: resAnswers });
        });
    });
  },
  create: function(req, res) {
    let surveyId = req.body.surveyId;
    let userId = req._id;

    AppliedForm.create({ userId, surveyId })
      .then(resAppliedForm => {
        res.status(200).json(resAppliedForm);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  },
  update: function(req, res) {
    let id = req.params.id;
    AppliedForm.findByIdAndUpdate(id, req.body)
      .then(resForm => {
        res.status(200).json(resForm);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  }
};
