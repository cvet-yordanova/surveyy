const passport = require("passport");

var User = require("../models/user");
var_ = require("lodash");

module.exports = {
  register: function(req, res, next) {
    var user = new User();
	
	console.log(req.body);

    user.fullName = req.body.fullName;
    user.email = req.body.email;
    user.password = req.body.password;

    user.save((err, doc) => {
      if (!err) {
        res.send(doc);
      } else {
        if (err.code === 11000) {
          res.status;
        } else return next(err);
      }
    });
  },
  authenticate: (req, res, next) => {
    passport.authenticate("local", (err, user, info) => {
      if (err) {
        return res.status(400).json(err);
      } else if (user) {
        return res.status(200).json({ token: user.generateJwt() });
      } else {
        return res.status(404).json(info);
      }
    })(req, res, next);
  },
  userProfile: (req, res, next) => {
    User.findOne({ _id: req._id }, (err, user) => {
      if (err) {
        return res
          .status(404)
          .json({ status: false, message: "User not found." });
      } else {
        return res
          .status(200)
          .json({ status: true, user: _.pick(user, ["fullName", "email"]) });
      }
    });
  }
};
