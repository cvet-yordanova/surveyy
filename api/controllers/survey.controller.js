const mongoose = require("mongoose");
const Survey = require("../models/survey");
const Question = require("../models/question");
const User = require("../models/user");
const Answer = require("../models/answer");
const async = require("async");

module.exports = {
  findAll: async function(req, res) {
    if (mongoose.connection.readyState == 1) {
      let result = await mongoose.connection.db
        .collection("surveys")
        .aggregate([
          {
            $lookup: {
              from: "questions",
              localField: "_id",
              foreignField: "survey",
              as: "questions"
            }
          }
        ])
        .toArray();

      res.status(200).json(result);
    }
  },
  findById: async function(req, res) {
    if (mongoose.connection.readyState == 1) {
      Survey.findById(req.params.id).then(survey => {
        let resultSurvey = { ...survey._doc };

        Question.find({ surveyId: { $eq: resultSurvey._id } }).then(
          async questions => {
            const promises = questions.map(async question => {
              let res = {};
              await Answer.find({ questionId: question._id }).then(
                resAnswers => {
                  res = { ...question._doc, answers: resAnswers };
                }
              );

              return res;
            });

            resultSurvey.questions = await Promise.all(promises);
            res.json(resultSurvey);
          }
        );
      });
    }
  },
  create: function(req, res) {
    let { title, description, dateCreated } = req.body;
    let userId = req._id;
    console.log(userId);
    Survey.create({ title, description, dateCreated, userId })
      .then(survey => res.json(survey))
      .catch(err => res.status(500).json(err));
  },
  deleteCascade: async function(req, res) {
    let surveyId = req.params.id;

    let surveyById;
    let answersByQuestion = [];
    let questionsByForm = [];

    await Question.find({ surveyId: { $eq: surveyId } }).then(res => {
      res.forEach(resQuestion => {
        questionsByForm.push(resQuestion);

        Answer.find({ questionId: { $eq: resQuestion._id } }).then(
          resAnswers => {
            resAnswers.forEach(resAnswer => {
              Answer.findByIdAndDelete(resAnswer._id).then(a => {
                answersByQuestion.push(a);
              });
            });
          }
        );
      });
    });

    questionsByForm.forEach(question => {
      Question.findByIdAndDelete(question._id).then();
    });

    await Survey.findByIdAndDelete(surveyId)
      .then(resSurvey => {
        surveyById = resSurvey;
        res.json({
          survey: surveyById,
          answers: answersByQuestion,
          questions: questionsByForm
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  }
};
