const Answer = require("../models/answer");

module.exports = {
  findByQuestionId: function(req, res) {
    Answer.find({ question: { $eq: req.params.questionId } }).then(
      (answers, err) => {
        if (err) {
          res.status(500);
        } else {
          res.json(answers);
        }
      }
    );
  },
  save: function(req, res) {
    const { id } = req.params;
    Answer.findByIdAndUpdate({ _id: id }, req.body, { new: true })
      .then(answer => res.json(answer))
      .catch(err => res.status(500).json(err));
  },

  create: function(req, res) {
    let { value, questionId, fieldTypeId } = req.body;
    Answer.create({ value, questionId, fieldTypeId })
      .then(answer => res.json(answer))
      .catch(err => res.status(500).json(err));
  },
  delete: function(req, res) {
    let { id } = req.params;
    Answer.deleteOne({ _id: id })
      .then(answer => res.json(answer))
      .catch(err => res.status(500).json(err));
  }
};
