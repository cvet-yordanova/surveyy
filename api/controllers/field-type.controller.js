const FieldType = require("../models/field-type");

module.exports = {
  findAll: function(req, res) {
    FieldType.find({}).then((resultFieldTypes, err) => {
      if (err) {
      }

      res.json(resultFieldTypes);
    });
  },
  findById: function(req, res) {
    FieldType.findById(req.params.id).then((resultFieldType, err) => {
      if (err) {
        //todo
      }

      res.json(resultFieldType);
    });
  }
};
