const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AnswerSchema = new Schema({
  value: {
    type: String,
    required: true
  },
  questionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Question"
  },
  fieldTypeId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "FieldType"
  }
});

module.exports = Answer = mongoose.model("Answer", AnswerSchema);
