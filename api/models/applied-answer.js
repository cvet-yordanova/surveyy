const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AppliedAnswerSschema = new Schema({
  questionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Question",
    required: true
  },
  answerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Answer"
  },
  appliedFormId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "AppliedForm"
  },
  textValue: {
    type: String
  },
  answerIds: {
    type: Array
  }
});

module.exports = AppliedAnswer = mongoose.model(
  "AppliedAnswer",
  AppliedAnswerSschema
);
