const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  order: {
    type: Number,
    required: true
  },
  required: {
    type: Boolean,
    required: true
  },
  surveyId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  }
});

module.exports = Question = mongoose.model("Question", QuestionSchema);
