const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AppliedFormSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  surveyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Survey",
    required: true
  }
});

module.exports = AppliedForm = mongoose.model("AppliedForm", AppliedFormSchema);
