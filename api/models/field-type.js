const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FieldTypeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  isPredifined: {
    type: Boolean,
    required: true
  }
});

module.exports = FieldType = mongoose.model("FieldType", FieldTypeSchema);
