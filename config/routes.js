const express = require("express");
const questionController = require("../api/controllers/question.controller");
const surveyController = require("../api/controllers/survey.controller");
const answerController = require("../api/controllers/answer.controller");
const fieldTypeController = require("../api/controllers/field-type.controller");
const userController = require("../api/controllers/user.controller");
const appliedFormController = require("../api/controllers/applied-form.controller");
const appliedAnswerController = require("../api/controllers/applied-answer.controller");

const jwtHelper = require("../config/jwt-helper");

module.exports = router = express.Router();

router.get("/surveys", jwtHelper.verifyJwtToken, surveyController.findAll);
router.get("/surveys/:id", jwtHelper.verifyJwtToken, surveyController.findById);
router.post("/surveys", jwtHelper.verifyJwtToken, surveyController.create);
router.delete(
  "/surveys/:id",
  jwtHelper.verifyJwtToken,
  surveyController.deleteCascade
);

router.get(
  "/answers-to-question/:questionId",
  jwtHelper.verifyJwtToken,
  answerController.findByQuestionId
);
router.get(
  "/field-type",
  jwtHelper.verifyJwtToken,
  fieldTypeController.findAll
);
router.get(
  "/field-type/:id",
  jwtHelper.verifyJwtToken,
  fieldTypeController.findById
);

router.put("/questions/:id", jwtHelper.verifyJwtToken, questionController.save);
router.post(
  "/questions/create",
  jwtHelper.verifyJwtToken,
  questionController.create
);
router.delete(
  "/questions/:id",
  jwtHelper.verifyJwtToken,
  questionController.delete
);

router.put("/answers/:id", jwtHelper.verifyJwtToken, answerController.save);
router.post(
  "/answers/create",
  jwtHelper.verifyJwtToken,
  answerController.create
);
router.delete(
  "/answers/:id",
  jwtHelper.verifyJwtToken,
  answerController.delete
);

router.post("/accounts", userController.register);
router.post("/authenticate", userController.authenticate);
router.post("/user", jwtHelper.verifyJwtToken, userController.userProfile);

router.get(
  "/applied-forms/:surveyId",
  jwtHelper.verifyJwtToken,
  appliedFormController.findBySurveyId
);
router.post(
  "/applied-forms",
  jwtHelper.verifyJwtToken,
  appliedFormController.create
);
router.put(
  "/applied-forms/:id",
  jwtHelper.verifyJwtToken,
  appliedFormController.update
);

router.post(
  "/applied-answers/",
  jwtHelper.verifyJwtToken,
  appliedAnswerController.create
);
router.put(
  "/applied-answers/:id",
  jwtHelper.verifyJwtToken,
  appliedAnswerController.update
);
